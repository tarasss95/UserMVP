﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.QualityTools.UnitTestFramework;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using UserMVP;

namespace UserMVP.Test
{
    [TestClass]
    public sealed class FileManagerTest
    {
        private static IFileManager fileManager;
        private static IUser user;

        [ClassInitialize]
        public static void InitializeClassTest(TestContext testContext)
        {
            fileManager=new FileManager();
            user=new User("new","new",22,"workplace");
        }
       
        [TestMethod]
        public void IsExistFile_ExistPathFile_True()
        {
            string fpath = "test.csv";
            bool expected = true;
            using (File.Create(fpath))
            {}
            bool actual = fileManager.IsExistFile(fpath);
            File.Delete(fpath);
            Assert.AreEqual(expected,actual);
        }

        [TestMethod]
        public void IsExistFile_NotExistPathFile_False()
        {
            string fpath = "test.csv";
            bool expected = false;
            bool actual = fileManager.IsExistFile(fpath);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsExistDirectory_ExistPathDirectory_True()
        {
            string dpath = "C:\\Program Files\\ffffff";
            bool expected = true;
            bool actual = fileManager.IsExistDirectory(dpath);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        public void IsExistDirectory_NotExistPathDirectory_False()
        {
            string dpath = "C:\\Program Files\\ffffff\\ddddd";
            bool expected = false;
            bool actual = fileManager.IsExistDirectory(dpath);
            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        [DeploymentItem("Files\\new.csv")]
        [DeploymentItem("Files\\new1.csv")]
        public void AddUser_AddUserToFile_ReadFromFileAndCompareWithTamplate()
        {
            string expected = File.ReadAllText("new.csv");
            fileManager.AddUser(user, "new1.csv");
            string actual = File.ReadAllText("new1.csv");
            Assert.AreEqual(expected,actual);
        }
    }
}
