﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.TypeConversion;
using CsvHelper.Configuration;

namespace UserMVP
{
    class CSVSerializer:ISerializer
    {
        public void Serialize<T>(StreamWriter sw,T obj)
        {
            using (CsvWriter csv = new CsvWriter(sw))
            {
                csv.Configuration.HasHeaderRecord = false;
                csv.Configuration.Delimiter = ",";
                csv.WriteRecord(obj);
            }
        }
    }
}
