﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ninject.Modules;

namespace UserMVP
{
    class ConfigModule:NinjectModule
    {
        
        public override void Load()
        {
            Bind<IUser>().To<User>();
            Bind<IMainForm>().To<MainForm>();
            Bind<IFileManager>().To<FileManager>();
            Bind<IMessageService>().To<MessageService>();
            Bind<ISettingForm>().To<SettingForm>();
            Bind<MainPresenter>().ToSelf();
            //Bind<MainForm>().ToSelf();
        }
    }
}
