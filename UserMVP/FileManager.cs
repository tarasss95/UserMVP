﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UserMVP
{
    public class FileManager:IFileManager
    {
        private readonly Encoding defaultEncoding = Encoding.GetEncoding(1251);

        private string DirectoryPath(string path)
        {
            int i = path.Length-1;
            while (path[i] != '\\')
            {
                i--;
            }
            return path.Remove(i , (path.Length - i));
        }
        public bool IsExistFile(string path)
        {
            bool isExist = File.Exists(path);
            return isExist;
        }

        public bool IsExistDirectory(string path)
        {
            bool isExistDirectory = Directory.Exists(DirectoryPath(path));
            return isExistDirectory;
        }

        public void CreateFile(string path)
        {
            using (File.Create(path))
            { }
             
        }

        public void CreateDirectory(string path)
        {
            Directory.CreateDirectory(DirectoryPath(path));
        }

        public void AddUser(IUser user, string path)
        {
            AddUser(user, path, defaultEncoding);
        }
        public void AddUser(IUser user,string path,Encoding encoding)
        {
            using (StreamWriter sw = new StreamWriter(path, true, encoding))
            {
                ISerializer csv=new CSVSerializer();
                csv.Serialize(sw,user);
            }
        }
    }
}
