﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserMVP
{
    public interface IFileManager
    {
        bool IsExistFile(string path);
        bool IsExistDirectory(string path);
        void CreateFile(string path);
        void CreateDirectory(string path);
        void AddUser(IUser user, string path);
        void AddUser(IUser user, string path, Encoding encoding);

    }
}
