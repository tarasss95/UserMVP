﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace UserMVP
{
    interface ISerializer
    {
        void Serialize<T>(StreamWriter sw, T obj);
    }
}
