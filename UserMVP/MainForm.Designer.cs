﻿namespace UserMVP
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.AddUserButton = new System.Windows.Forms.Button();
            this.TBLastName = new System.Windows.Forms.TextBox();
            this.TBFirstName = new System.Windows.Forms.TextBox();
            this.TBAge = new System.Windows.Forms.TextBox();
            this.TBWorkPlace = new System.Windows.Forms.TextBox();
            this.LastNameLabel = new System.Windows.Forms.Label();
            this.FirstNameLabel = new System.Windows.Forms.Label();
            this.AgeLabel = new System.Windows.Forms.Label();
            this.WorkPlaceLabel = new System.Windows.Forms.Label();
            this.RTBInfo = new System.Windows.Forms.RichTextBox();
            this.ExitButton = new System.Windows.Forms.Button();
            this.SettingButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // AddUserButton
            // 
            this.AddUserButton.Location = new System.Drawing.Point(12, 240);
            this.AddUserButton.Name = "AddUserButton";
            this.AddUserButton.Size = new System.Drawing.Size(143, 27);
            this.AddUserButton.TabIndex = 0;
            this.AddUserButton.Text = "Додати користувача";
            this.AddUserButton.UseVisualStyleBackColor = true;
            // 
            // TBLastName
            // 
            this.TBLastName.Location = new System.Drawing.Point(117, 42);
            this.TBLastName.Name = "TBLastName";
            this.TBLastName.Size = new System.Drawing.Size(232, 20);
            this.TBLastName.TabIndex = 1;
            // 
            // TBFirstName
            // 
            this.TBFirstName.Location = new System.Drawing.Point(117, 73);
            this.TBFirstName.Name = "TBFirstName";
            this.TBFirstName.Size = new System.Drawing.Size(232, 20);
            this.TBFirstName.TabIndex = 2;
            // 
            // TBAge
            // 
            this.TBAge.Location = new System.Drawing.Point(117, 109);
            this.TBAge.Name = "TBAge";
            this.TBAge.Size = new System.Drawing.Size(88, 20);
            this.TBAge.TabIndex = 3;
            // 
            // TBWorkPlace
            // 
            this.TBWorkPlace.Location = new System.Drawing.Point(117, 146);
            this.TBWorkPlace.Name = "TBWorkPlace";
            this.TBWorkPlace.Size = new System.Drawing.Size(232, 20);
            this.TBWorkPlace.TabIndex = 4;
            // 
            // LastNameLabel
            // 
            this.LastNameLabel.AutoSize = true;
            this.LastNameLabel.Location = new System.Drawing.Point(52, 45);
            this.LastNameLabel.Name = "LastNameLabel";
            this.LastNameLabel.Size = new System.Drawing.Size(59, 13);
            this.LastNameLabel.TabIndex = 5;
            this.LastNameLabel.Text = "Прізвище:";
            // 
            // FirstNameLabel
            // 
            this.FirstNameLabel.AutoSize = true;
            this.FirstNameLabel.Location = new System.Drawing.Point(79, 76);
            this.FirstNameLabel.Name = "FirstNameLabel";
            this.FirstNameLabel.Size = new System.Drawing.Size(32, 13);
            this.FirstNameLabel.TabIndex = 6;
            this.FirstNameLabel.Text = "Ім\'я: ";
            // 
            // AgeLabel
            // 
            this.AgeLabel.AutoSize = true;
            this.AgeLabel.Location = new System.Drawing.Point(83, 112);
            this.AgeLabel.Name = "AgeLabel";
            this.AgeLabel.Size = new System.Drawing.Size(28, 13);
            this.AgeLabel.TabIndex = 7;
            this.AgeLabel.Text = "Вік: ";
            // 
            // WorkPlaceLabel
            // 
            this.WorkPlaceLabel.AutoSize = true;
            this.WorkPlaceLabel.Location = new System.Drawing.Point(34, 149);
            this.WorkPlaceLabel.Name = "WorkPlaceLabel";
            this.WorkPlaceLabel.Size = new System.Drawing.Size(77, 13);
            this.WorkPlaceLabel.TabIndex = 8;
            this.WorkPlaceLabel.Text = "Місце роботи:";
            // 
            // RTBInfo
            // 
            this.RTBInfo.Location = new System.Drawing.Point(473, 12);
            this.RTBInfo.Name = "RTBInfo";
            this.RTBInfo.ReadOnly = true;
            this.RTBInfo.Size = new System.Drawing.Size(236, 200);
            this.RTBInfo.TabIndex = 9;
            this.RTBInfo.Text = "";
            // 
            // ExitButton
            // 
            this.ExitButton.Location = new System.Drawing.Point(610, 240);
            this.ExitButton.Name = "ExitButton";
            this.ExitButton.Size = new System.Drawing.Size(99, 27);
            this.ExitButton.TabIndex = 10;
            this.ExitButton.Text = "Вихід";
            this.ExitButton.UseVisualStyleBackColor = true;
            this.ExitButton.Click += new System.EventHandler(this.ExitButton_Click);
            // 
            // SettingButton
            // 
            this.SettingButton.Location = new System.Drawing.Point(225, 240);
            this.SettingButton.Name = "SettingButton";
            this.SettingButton.Size = new System.Drawing.Size(179, 27);
            this.SettingButton.TabIndex = 15;
            this.SettingButton.Text = "Настройки файлу";
            this.SettingButton.UseVisualStyleBackColor = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(719, 279);
            this.Controls.Add(this.SettingButton);
            this.Controls.Add(this.ExitButton);
            this.Controls.Add(this.RTBInfo);
            this.Controls.Add(this.WorkPlaceLabel);
            this.Controls.Add(this.AgeLabel);
            this.Controls.Add(this.FirstNameLabel);
            this.Controls.Add(this.LastNameLabel);
            this.Controls.Add(this.TBWorkPlace);
            this.Controls.Add(this.TBAge);
            this.Controls.Add(this.TBFirstName);
            this.Controls.Add(this.TBLastName);
            this.Controls.Add(this.AddUserButton);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Додавання користувачів";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button AddUserButton;
        private System.Windows.Forms.TextBox TBLastName;
        private System.Windows.Forms.TextBox TBFirstName;
        private System.Windows.Forms.TextBox TBAge;
        private System.Windows.Forms.TextBox TBWorkPlace;
        private System.Windows.Forms.Label LastNameLabel;
        private System.Windows.Forms.Label FirstNameLabel;
        private System.Windows.Forms.Label AgeLabel;
        private System.Windows.Forms.Label WorkPlaceLabel;
        private System.Windows.Forms.RichTextBox RTBInfo;
        private System.Windows.Forms.Button ExitButton;
        private System.Windows.Forms.Button SettingButton;
    }
}

