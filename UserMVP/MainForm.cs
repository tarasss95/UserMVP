﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserMVP
{
    public interface IMainForm
    {
        //string FilePath { get; }
        string UserInfo { set; }
        string UserFirstName { get; set; }
        string UserLastName { get; set; }
        int UserAge { get; set; }
        string UserWorkPlace { get; set; }
        

        event EventHandler AddUserClick;
        event EventHandler OpenSettingForm;
        //event EventHandler CreateFile;

    }
    public partial class MainForm : Form,IMainForm
    {
        public event EventHandler AddUserClick;
        public event EventHandler OpenSettingForm;
        //public event EventHandler CreateFile;
        public MainForm()
        {
            InitializeComponent();
            AddUserButton.Click += AddUserButton_Click;
            SettingButton.Click += SettingButton_Click;

            //SelectButton.Click += SelectButton_Click;
            //CreateButton.Click += CreateButton_Click;
        }

        private void SettingButton_Click(object sender, EventArgs e)
        {
            if (OpenSettingForm != null)
            {
                OpenSettingForm(this,EventArgs.Empty);
            }
        
        }

        private void AddUserButton_Click(object sender, EventArgs e)
        {
            if (AddUserClick != null)
            {
                AddUserClick(this, EventArgs.Empty);
            }
        }

        

        public string UserInfo
        {
            set { RTBInfo.AppendText(value+Environment.NewLine); }
        }

        public string UserFirstName
        {
            get { return TBFirstName.Text; }
            set { TBFirstName.Text = value; }
        }
        public string UserLastName
        {
            get { return TBLastName.Text; }
            set { TBLastName.Text = value; }
        }
        public int UserAge
        {
            get { return Convert.ToInt32(TBAge.Text); }
            set { TBLastName.Text = value.ToString(); }
        }
        public string UserWorkPlace
        {
            get { return TBWorkPlace.Text; }
            set { TBWorkPlace.Text = value; }
        }

        
        

        

        private void ExitButton_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
       
    }
}
