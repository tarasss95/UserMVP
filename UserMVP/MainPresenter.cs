﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserMVP
{
    public class MainPresenter
    {
        private  readonly IMainForm mainForm;
        private  readonly IFileManager fileManager;
        private  readonly IMessageService messageService;
        private  readonly ISettingForm settingForm;
        private  IUser user;
        private SingletonSetting singletonSetting;
        private string CurrentPath;

        //public MainPresenter()
        //{
        //    this.mainForm = new MainForm();
        //    this.settingForm = new SettingForm();
        //    this.fileManager = new FileManager();
        //    this.messageService = new MessageService();
        //    this.mainForm.AddUserClick += MainForm_AddUserClick;
        //    this.mainForm.OpenSettingForm += MainForm_OpenSettingForm;
        //    this.settingForm.CreateFile += SettingForm_CreateFile;
        //    this.settingForm.OK += SettingForm_OK;
        //}
        public MainPresenter(IMainForm mainForm, ISettingForm settingForm, IFileManager fileManager, IMessageService messageService)
        {
            this.mainForm = mainForm;
            this.settingForm = settingForm;
            this.fileManager = fileManager;
            this.messageService = messageService;
            this.mainForm.AddUserClick += MainForm_AddUserClick;
            this.mainForm.OpenSettingForm += MainForm_OpenSettingForm;
            this.settingForm.CreateFile += SettingForm_CreateFile;
            this.settingForm.OK += SettingForm_OK;
            //settingPresenter=new SettingPresenter(settingForm, fileManager,messageService);
            //this.mainForm.CreateFile += MainForm_CreateFile;

        }

        private void MainForm_OpenSettingForm(object sender, EventArgs e)
        {
                settingForm.SettingShow();
        }
        private void SettingForm_OK(object sender, EventArgs e)
        {
            try
            {
                singletonSetting = SingletonSetting.GetSingletonSetting(settingForm.FilePath);
                settingForm.SettingClose();
            }
            catch (Exception ex)
            {
                messageService.ShowError(ex.Message);
            }

        }
        private void SettingForm_CreateFile(object sender, EventArgs e)
        {
            try
            {
                CurrentPath = settingForm.FilePath;
                if (!fileManager.IsExistDirectory(CurrentPath))
                {
                    fileManager.CreateDirectory(CurrentPath);
                }
                if (!fileManager.IsExistFile(CurrentPath))
                {
                    fileManager.CreateFile(CurrentPath);
                    messageService.ShowMessage("Файл створено.");
                }
            }
            catch (Exception ex)
            {
                messageService.ShowError(ex.Message);

            }
        }
        

        private void MainForm_AddUserClick(object sender, EventArgs e)
        {
            try
            {
                CurrentPath = singletonSetting.SettingPath;
                user=new User(mainForm.UserFirstName, mainForm.UserLastName, mainForm.UserAge, mainForm.UserWorkPlace);
                if (!fileManager.IsExistFile(CurrentPath))
                {
                    messageService.ShowExclamation("Файл не існує! Створіть його!");
                }
                else
                {
                    fileManager.AddUser(user, CurrentPath);
                    mainForm.UserInfo = DateTime.Now + " додано користувача '" + user.FirstName + " " + user.LastName + "'.";
                    messageService.ShowMessage("Користувач записаний у файл.");
                }
                
            }
            catch (Exception ex)
            {
                messageService.ShowError(ex.Message);
            }
        }
    }
}
