﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Ninject;

namespace UserMVP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            
            IKernel ninjectKernel = new StandardKernel(new ConfigModule());

            IMainForm mainForm = ninjectKernel.Get<IMainForm>();
            ISettingForm settingForm = ninjectKernel.Get<ISettingForm>();
            IFileManager fileManager = ninjectKernel.Get<IFileManager>();
            IMessageService messageService = ninjectKernel.Get<IMessageService>();
            MainPresenter mainPresenter = new MainPresenter(mainForm, settingForm, fileManager, messageService);

            Application.Run(mainForm as MainForm);
            
        }
    }
}
