﻿namespace UserMVP
{
    partial class SettingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PathLabel = new System.Windows.Forms.Label();
            this.CreateButton = new System.Windows.Forms.Button();
            this.SelectButton = new System.Windows.Forms.Button();
            this.TBFilePath = new System.Windows.Forms.TextBox();
            this.OkButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // PathLabel
            // 
            this.PathLabel.AutoSize = true;
            this.PathLabel.Location = new System.Drawing.Point(12, 20);
            this.PathLabel.Name = "PathLabel";
            this.PathLabel.Size = new System.Drawing.Size(88, 13);
            this.PathLabel.TabIndex = 13;
            this.PathLabel.Text = "Шлях до файлу: ";
            // 
            // CreateButton
            // 
            this.CreateButton.Location = new System.Drawing.Point(245, 54);
            this.CreateButton.Name = "CreateButton";
            this.CreateButton.Size = new System.Drawing.Size(113, 23);
            this.CreateButton.TabIndex = 17;
            this.CreateButton.Text = "Створити файл";
            this.CreateButton.UseVisualStyleBackColor = true;
            // 
            // SelectButton
            // 
            this.SelectButton.Location = new System.Drawing.Point(103, 54);
            this.SelectButton.Name = "SelectButton";
            this.SelectButton.Size = new System.Drawing.Size(75, 23);
            this.SelectButton.TabIndex = 16;
            this.SelectButton.Text = "Вибрати";
            this.SelectButton.UseVisualStyleBackColor = true;
            // 
            // TBFilePath
            // 
            this.TBFilePath.Location = new System.Drawing.Point(103, 17);
            this.TBFilePath.Name = "TBFilePath";
            this.TBFilePath.Size = new System.Drawing.Size(309, 20);
            this.TBFilePath.TabIndex = 15;
            // 
            // OkButton
            // 
            this.OkButton.Location = new System.Drawing.Point(171, 87);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(91, 23);
            this.OkButton.TabIndex = 18;
            this.OkButton.Text = "Добре";
            this.OkButton.UseVisualStyleBackColor = true;
            // 
            // SettingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(476, 122);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.CreateButton);
            this.Controls.Add(this.SelectButton);
            this.Controls.Add(this.TBFilePath);
            this.Controls.Add(this.PathLabel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Настройки файла";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PathLabel;
        private System.Windows.Forms.Button CreateButton;
        private System.Windows.Forms.Button SelectButton;
        private System.Windows.Forms.TextBox TBFilePath;
        private System.Windows.Forms.Button OkButton;
    }
}