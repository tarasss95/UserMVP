﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UserMVP
{
    public interface ISettingForm
    {
        string FilePath { get; }
        event EventHandler CreateFile;
        event EventHandler OK;
        void SettingShow();
        void SettingClose();
    }
    public partial class SettingForm : Form,ISettingForm
    {
        public event EventHandler CreateFile;
        public event EventHandler OK;
        public void SettingShow()
        {
            this.ShowDialog();
        }

        public void SettingClose()
        {
            
            this.Close();
        }
        public string FilePath
        {
            get { return TBFilePath.Text; }
        }
        public SettingForm()
        {
            InitializeComponent();
            SelectButton.Click += SelectButton_Click;
            CreateButton.Click += CreateButton_Click;
            OkButton.Click += OkButton_Click;
            //TBFilePath.TextChanged += TBFilePath_TextChanged;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            if (OK != null)
            {
                OK(this,EventArgs.Empty);
            }
        }


        private void CreateButton_Click(object sender, EventArgs e)
        {
            if (CreateFile != null)
            {
                CreateFile(this, EventArgs.Empty);
            }
        }
        private void SelectButton_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстові файли|*.csv|Всі файли|*.*";
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                TBFilePath.Text = openFileDialog.FileName;
            }
            //SettingForm f=new SettingForm();

         
        }
        
    }
}
