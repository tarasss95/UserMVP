﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserMVP
{
    public class SingletonSetting
    {
        private static SingletonSetting singletonSetting;
        public string SettingPath { get; private set; }

        
        protected SingletonSetting(string path)
        {
            this.SettingPath = path;
        }
        public static SingletonSetting GetSingletonSetting(string path)
        {
            if (singletonSetting == null)
            {
                singletonSetting = new SingletonSetting(path);
            }
            return singletonSetting;
        }
    }
}
