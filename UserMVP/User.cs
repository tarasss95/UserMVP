﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserMVP
{
    public class User:IUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
        public string WorkPlace { get; set; }
        public User()
        {
            this.FirstName = String.Empty;
            this.LastName = String.Empty;
            this.Age = 0;
            this.WorkPlace = String.Empty;
        }
        public User(string firstname, string lastname, int age, string workplace)
        {
            this.FirstName = firstname;
            this.LastName = lastname;
            this.Age = age;
            this.WorkPlace = workplace;
        }

        public override string ToString()
        {
            return LastName + " " + FirstName + " " + Age.ToString() + " " + WorkPlace;
        }
    }
}
